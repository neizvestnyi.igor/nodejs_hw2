const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
});
// In which collection (notes) this model will be stored:
const Note = mongoose.model('notes', noteSchema);

module.exports = {
  Note,
};
