const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  _id: {
    type: String,
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
});
// In which collection (users) this model will be stored:
const User = mongoose.model('users', userSchema);

module.exports = {
  User,
};
