const mongoose = require('mongoose');

// In which collection (credentials) this model will be stored:
const Credentials = mongoose.model('credentials', {
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
});

module.exports = {
  Credentials,
};
