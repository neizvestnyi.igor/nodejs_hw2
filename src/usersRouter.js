const express = require('express');

const router = express.Router();
const {
  registerUser, loginUser, getUser, deleteUser, patchUser,
} = require('./usersService');
const {authMiddleware} = require('./middlewares/authMiddleware');

// Registration of request/http methods in the router:
router.post('/auth/register', registerUser);
router.post('/auth/login', loginUser);
router.get('/users/me', authMiddleware, getUser);
router.delete('/users/me', authMiddleware, deleteUser);
router.patch('/users/me', authMiddleware, patchUser);

module.exports = {
  usersRouter: router,
};
