const express = require('express');

const router = express.Router();
const {
  createNote,
  getNote,
  updateMyNoteById,
  markMyNoteCompleted,
  deleteNote,
  getNotes,
} = require('./notesService');

// Checking if the user is logged in:
const {authMiddleware} = require('./middlewares/authMiddleware');

router.get('/', authMiddleware, getNotes);
router.post('/', authMiddleware, createNote);
router.get('/:id', authMiddleware, getNote);
router.put('/:id', authMiddleware, updateMyNoteById);
router.patch('/:id', authMiddleware, markMyNoteCompleted);
router.delete('/:id', authMiddleware, deleteNote);

module.exports = {
  notesRouter: router,
};
