const express = require('express'); // Server
const morgan = require('morgan'); // Logger middleware
require('dotenv').config(); // .env connect

const app = express(); // Create server app
const mongoose = require('mongoose'); // ORM

mongoose.connect(process.env.DB_PATH); // From .env

const {usersRouter} = require('./usersRouter');
const {notesRouter} = require('./notesRouter');

app.use(express.json()); // json from body to req.body:
app.use(morgan('tiny')); // Every request info to logs

// For different URLs - corresponding routers:
app.use('/api/', usersRouter);
app.use('/api/notes', notesRouter);

const start = async () => {
  try {
    app.listen(8080);
    console.log('Server running on port: 8080');
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
/**
 *
 * @param {*} err Object
 * @param {*} res Object
 */
function errorHandler(err, res) {
  console.error(err);
  res.status(500).send({message: 'Server error'});
}

// ERROR HANDLER
app.use(errorHandler);
