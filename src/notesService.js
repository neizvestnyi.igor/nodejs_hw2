const {Note} = require('./models/Note');

const createNote = (req, res) => {
  const {userId} = req.user;
  const {text} = req.body;
  const note = new Note({
    userId,
    text,
  });
  note.save().then((saved) => {
    res.json({message: 'Success'});
  }).catch((err) => {
    res.status(400).send({'message': `${err}`});
  });
};

const getNote = (req, res) => Note.findById(req.params.id, '-__v')
    .then((note) => {
      res.json({
        'note': note,
      });
    }).catch((err) => {
      res.status(400).send({'message': `${err}`});
    });

const deleteNote = (req, res) => Note.findByIdAndDelete(req.params.id)
    .then(() => {
      res.json({message: 'Success'});
    }).catch((err) => {
      res.status(400).send({'message': `${err}`});
    });

const getNotes = (req, res) => {
  console.log(req);
  Note.find({userId: req.user.userId}, '-__v').then((result) => {
    res.json({
      'offset': +req.query.offset,
      'limit': +req.query.limit,
      'count': result.length,
      'notes': result,
    });
  }).catch((err) => {
    res.status(400).send({'message': `${err}`});
  });
};

const updateMyNoteById = (req, res) => {
  const {text} = req.body;
  Note.findByIdAndUpdate({
    _id: req.params.id,
    userId: req.user.userId,
  }, {$set: {text}})
      .then(() => {
        res.json({message: 'Success'});
      }).catch((err) => {
        res.status(400).send({'message': `${err}`});
      });
};

const markMyNoteCompleted = async (req, res) => {
  const note = await Note.findById(req.params.id);
  Note.findByIdAndUpdate({
    _id: req.params.id, // ID from url
    userId: req.user.userId, // Object from middleware for current user
  }, {$set: {completed: !note.completed}})
      .then(() => {
        res.json({message: 'Success'});
      }).catch((err) => {
        res.status(400).send({'message': `${err}`});
      });
};

module.exports = {
  createNote,
  getNotes,
  getNote,
  deleteNote,
  updateMyNoteById,
  markMyNoteCompleted,
};
