const bcrypt = require('bcryptjs'); // Password hashing function
const jwt = require('jsonwebtoken');
require('dotenv').config();

// Mongoose models:
const {Credentials} = require('./models/Credentials');
const {User} = require('./models/User');

const registerUser = async (req, res, next) => {
  const {username, password} = req.body;

  if (!username || !password) {
    res.status(400).send({'message': `Input correct data`});
  }

  // Create new user in 'credentials':
  const user = new Credentials({
    username,
    password: await bcrypt.hash(password, 10),
  });

  // Save user:
  user.save()
      .then((saved) => {
        // Create new user in 'users':
        const userBD = new User({
          _id: saved._id,
          username,
        });
        userBD.save().then(() => {
          res.status(200).send({'message': 'Success'});
        });
      })
      .catch((err) => {
        res.status(400).send({'message': `${err}`});
      });
};

// Login, when we have user:
const loginUser = async (req, res) => {
  const user = await Credentials.findOne({username: req.body.username});
  // If we have a user:
  // .compare - compares passwords:
  if
  (user && await bcrypt
      .compare(String(req.body.password), String(user.password))) {
    // Create {payload} for jwt:
    const payload = {
      username: user.username, userId: user._id,
    };
    // Create token:
    const jwtToken = jwt.sign(payload, process.env.secretkey);
    return res.json({message: 'Success', jwt_token: jwtToken});
  }
  return res.status(400).json({message: 'Not authorized'});
};

const getUser = async (req, res) => {
  // Field from headers:
  const {authorization} = req.headers;
  // We should get 2 words:
  const [, token] = authorization.split(' ');
  const decoded = jwt.decode(token, process.env.secretkey);
  User.findById(decoded.userId)
      .then((user) => {
        res.json({'user': {
          _id: user._id,
          username: user.username,
          createdDate: user.createdDate,
        }});
      }) .catch((err) => {
        res.status(400).send({'message': `${err}`});
      });
};

const deleteUser = async (req, res) => {
  const {authorization} = req.headers;
  const [, token] = authorization.split(' ');
  const decoded = jwt.decode(token, process.env.secretkey);
  User.findByIdAndDelete(decoded.userId)
      .then(() => {
        // console.log(decoded);
        Credentials.findByIdAndDelete(decoded.userId).then(() => {
          res.status(200).send({'message': 'Success'});
        });
      })
      .catch((err) => {
        res.status(400).send({'message': `${err}`});
      });
};

const patchUser = async (req, res) => {
  const {newPassword} = req.body;
  const {authorization} = req.headers;
  const [, token] = authorization.split(' ');
  const decoded = jwt.decode(token, process.env.secretkey);
  const userCred = await Credentials.findById(decoded.userId);
  if (userCred) {
    Credentials
        .findByIdAndUpdate(decoded.userId,
            {$set: {password: await bcrypt.hash(newPassword, 10)}})
        .then(() => {
          res.status(200).send({'message': 'Success'});
        }).catch((err) => {
          res.status(400).send({'message': `${err}`});
        });
  } else {
    res.status(400).send({'message': `Error user`});
  }
};

module.exports = {
  registerUser,
  loginUser,
  getUser,
  deleteUser,
  patchUser,
};
